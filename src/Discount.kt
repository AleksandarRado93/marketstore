open class Discount(private val turnover: Double, private val purchaseValue: Double) {
    private var discountRate = 0.0
    private var discount = 0.0
    private var total = 0.0

    fun bronzeCard() {
        if (turnover in 100.0..300.0) discountRate = 1.0
        if (turnover > 300) discountRate = 2.5

        calculateDiscount()
        calculateTotal()

        printDiscount(purchaseValue)
    }

    fun silverCard() {
        discountRate = 2.0
        if (turnover > 300) discountRate = 3.5

        calculateDiscount()
        calculateTotal()

        printDiscount(purchaseValue)
    }


    fun goldCard() {
        discountRate = 2.0

        for (x in 100..turnover.toInt() step 100) {
            if (discountRate <= 9)
                discountRate++

        }

        calculateDiscount()
        calculateTotal()

        printDiscount(purchaseValue)

    }

    private fun calculateDiscount() {
        discount = (purchaseValue * discountRate) / 100
    }

    private fun calculateTotal() {
        total = purchaseValue - discount
    }

    private fun formatDouble(double: Double): String {
        return String.format("%.2f", double)
    }

    private fun printDiscount(purchaseValue: Double) {
        println("Purchase value: $${formatDouble(purchaseValue)}")
        println("Discount rate: $discountRate%")
        println("Discount: $${formatDouble(discount)}")
        println("Total $${formatDouble(total)}")
    }

}

